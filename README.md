# ASI API Coding Exercise

Implement following RESTful API to take the following parameters as input, perform the calculation, and return the desired result.

## General Requirements

1. Send your solution as a .zip to the hiring manager via email by the agreed to submission deadline.
1. The API should follow REST Standards
1. The API should be secure. Add simplest security mechanism of your choice and provide recommendation on production level security implementation.
1. The API should handle required input validations & errors and all errors should be written to a file e.g. 'api-errors.log'
1. Identify all mandatory inputs by looking at logic and add validations on them.
1. Unit tests are expected
1. Provide instructions for build and installation.

## API Requirements

Build an API that calculates the amount needed for retirement as well as the accumulation and decumulation of their wealth over time:

1. Take currentPreTaxIncome from request as “amt”
1. Start with currentAge and add 3% to “amt” till retirementAge
1. From retirementAge + 1 substract 5% from “amt” till deathAge
1. Final Amount (“targetAmount”) will be sum of all “amt” from retirementAge to deathAge

### Payload
```
{
 "currentAge": "30",
 "retirementAge" : "60",
 "deathAge": "90",
 "retirementAmount" : "50000",
 "currentPreTaxIncome" : "4000",
 "contributionAmount": "500",
 "contributionFrequency" : "Monthly"
}
```

### Response
```
{
 "targetAmount":"3621924",
 "wealthTrajectory": [
 	{"year": "2017", "value": "100000"},
 	{"year": "2018", "value": "103000"},	
	...
 ]
}
```
